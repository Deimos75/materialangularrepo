/* eslint-env es6 */

const bookList = [
    { title: 'The Hobbit', pageCount: 184, releaseDate: '1937'},
    { title: 'The Two Towers', pageCount: 294, releaseDate: '1954'},
    { title: 'The Return of the King', pageCount: 347, releaseDate: '1955'},
    { title: 'The Silmarillion', pageCount: 411, releaseDate: '1977'}
  ];

const author = {
    firstname: 'J.R.R',
    lastname: 'Tolkien'
};

// const bookWithAuthorList = { ...bookList[0], ...author };

const bookWithAuthorList = bookList.map((book) => { 
    return { ...author, ...book} 
});

console.log('===> bookWithAuthorList : ', bookWithAuthorList);
console.log('===> bookWithAuthorList : ', bookWithAuthorList);
//https://www.flaticon.com/free-icon/glass-of-water_3248258?term=glass+of+water&related_id=3248258
