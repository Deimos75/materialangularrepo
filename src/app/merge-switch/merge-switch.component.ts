import { Component, OnDestroy } from "@angular/core";
import { of, Subject } from 'rxjs';
import { concatMap, delay, mergeMap, switchMap, takeUntil } from "rxjs/operators";

@Component({
    selector: 'app-merge-switch',
    templateUrl: './merge-switch.component.html',
    styleUrls: ['./merge-switch.component.css']
})
export class MergeSwitchComponent implements OnDestroy {

    obs$ = of(4000, 1000);
    testTab?: number[];
    subRes?: number[];
    subDOM?: number[];
    concatMapRes?: number[];
    concatMapDOM?: number[];
    mergeMapRes?: number[];
    mergeMapDOM?: number[];
    switchMapRes?: number[];
    switchMapDOM?: number[];
    destroy$: Subject<boolean> = new Subject<boolean>();

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    effacer(): void {
        this.subRes = this.subDOM = undefined;
        this.concatMapRes = this.concatMapDOM = undefined;
        this.mergeMapRes = this.mergeMapDOM = undefined;
        this.switchMapRes = this.switchMapDOM = undefined;
        console.clear();
    }

    abonner(): void {
        // Simple subscribe
        this.obs$.pipe(takeUntil(this.destroy$)).subscribe((value: number) => {
            setTimeout(() => 
                {
                    console.log('Sub : ', value);
                    if(!this.subRes) {
                        this.subRes = [value];
                    } else {
                        this.subRes.push(value);
                    }
                    this.subDOM = this.subRes.slice();
                }, 
                value);
        });


        // concatMap souscrit à l'observable suivant que quand la précédente est terminée
        // donc les observables vont être émises dans l'ordre de souscription
        this.obs$
            .pipe(takeUntil(this.destroy$), concatMap(val => of(val).pipe(delay(val))))
            .subscribe((value: number) => {
                console.log('concatMap : ', value);
                if(!this.concatMapRes) {
                    this.concatMapRes = [value];
                } else {
                    this.concatMapRes.push(value);
                }
                // Il faut faire une copie dans une nouvelle variable pour qu'elle s'actualise dans le DOM
                this.concatMapDOM = this.concatMapRes.slice();
        });


        // mergeMap souscrit à toutes les observables directement et émet la première qui a finit
        // donc ici l'observable avec le delay le plus court va être émise d'abord
        this.obs$
            .pipe(takeUntil(this.destroy$), mergeMap(val => of(val).pipe(delay(val))))
            .subscribe((value: number) => {
                console.log('mergeMap : ', value);
                if(!this.mergeMapRes) {
                    this.mergeMapRes = [value];
                } else {
                    this.mergeMapRes.push(value);
                }
                // Il faut faire une copie dans une nouvelle variable pour qu'elle s'actualise dans le DOM
                this.mergeMapDOM = this.mergeMapRes.slice();
            });


        // switchMap se désabonne de l'observable dés qu'une valeur est reçu
        // Ici c'est la deuxième valeur (1000ms) qui est reçu en premier et switchMap se désabonne après cette valeur  
        this.obs$
        .pipe(takeUntil(this.destroy$), switchMap(val => of(val).pipe(delay(val))))
        .subscribe((value: number) => {
            console.log('switchMap : ', value);
            if(!this.switchMapRes) {
                this.switchMapRes = [value];
            } else {
                this.switchMapRes.push(value);
            }
            // Il faut faire une copie dans une nouvelle variable pour qu'elle s'actualise dans le DOM
            this.switchMapDOM = this.switchMapRes.slice();
        });
    }
}
