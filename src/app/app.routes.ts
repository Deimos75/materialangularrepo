import { Routes } from "@angular/router";
import { MaterialComponent } from './material/material.component';
import { MapComponent } from './map/map.component';
import { ChallengeComponent } from "./challenge/challenge.component";

export const ROUTES: Routes = [
    // {path: '', component: AccueilComponent},
    { path: '', pathMatch: 'full', redirectTo: '/material' },
    { path: 'material', component: MaterialComponent },
    { path: 'map', component: MapComponent },
    { path: 'challenge', component: ChallengeComponent }
];
