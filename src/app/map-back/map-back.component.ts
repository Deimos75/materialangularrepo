import { map, mergeMap, delay, take } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-back',
  templateUrl: './map-back.component.html',
  styleUrls: ['./map-back.component.css']
})
export class MapBackComponent implements OnInit, OnDestroy {

  user?: User[];
  idSelected?: number;
  userSelected?: User;
  userSelected$?: Observable<string|undefined>;
  rechercheByObs?: boolean;
  destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor() { }
  
  ngOnInit(): void {
    this.user = [{id:0, nom: 'Toto'}, {id:1, nom: 'Titi'}, {id:2, nom: 'Vincent'}];
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  rechercherSUB(): void {
    // Mauvaise pratique: Double subscribe
    // this.getId().pipe(takeUntil(this.destroy$)).subscribe((value: number) => {
    //   this.idSelected = value;
    //   console.log('SUB id : ', value);
    //   this.getUserById(value).subscribe((value: User|undefined) => {
    //     console.log('SUB user : ', value);
    //     this.userSelected = value;
    //   });
    // });

    // Bonne pratique: MergeMap avec un subscribe
    this.getId().pipe(
      take(1),
      mergeMap((valueId: number) => this.getUserById(valueId))
    ).pipe(take(1))
    .subscribe((valueUser: User|undefined) => this.userSelected = valueUser);
  }

  rechercherOBS(): void {
    // Bonne pratique: MergeMap sans subscribe
    this.userSelected$ = this.getId().pipe(
      mergeMap((valueId: number) => this.getUserById(valueId)),
      map((value: User|undefined) => value?.nom)
    );
    this.rechercheByObs = true;
  }

  effacer(): void {
    console.clear();
    this.userSelected = undefined;
    this.userSelected$ = undefined;
    this.rechercheByObs = false;
  }

  getId(): Observable<number> {
    return of(2).pipe(delay(4000));
  }

  getUserById(id: number): Observable<User | undefined> {
    if(this.user) {
      const user = this.user.find(elt => elt.id === id);
      console.log('user : ', user);
      return of(user).pipe(delay(1000));
    } else {
      return of(undefined);
    }
  }
}

interface User {
  id: number;
  nom: string;
}
