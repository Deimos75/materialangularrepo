import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'concat'
})
export class ConcatPipe implements PipeTransform {
  transform(value: string): string {
    return value
      .replace(/ /g, '')
      .replace(/[èéêë]/g, 'e')
      .replace(/[àâ]/g, 'a')
      .replace(/[ïî]/g, 'i')
      .replace(/[öô]/g, 'o')
      .replace(/[üù]/g, 'u');
  }
}
