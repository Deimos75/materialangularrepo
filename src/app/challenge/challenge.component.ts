import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.css']
})
export class ChallengeComponent implements OnInit {

  // Challenge 1
  arr = [1, 'John', 24, true];
  nombres: any[];
  somme: number | undefined;
  solutionChallenge1: boolean;
  inversion: any[];
  challenge1Form: FormGroup;
  tabSommeCtrl: FormControl;
  tabSommeObs?: Observable<any>;
  
  // Challenge 2
  num: number;
  solutionChallenge2: boolean;
  
  // Challenge 3
  smileArray: string[];
  nbSmiley: number;
  solutionChallenge3: boolean;

  // Challenge 4
  couleurAleatoire: string;
  solutionChallenge4: boolean;

  constructor(private fb: FormBuilder) {
    this.nombres = [];
    this.solutionChallenge1 = this.solutionChallenge2 = this.solutionChallenge3 = this.solutionChallenge4 = false;
    this.num = 348597;
    this.inversion = [];
    this.smileArray = [':)', ';}', ';-D', ';(', ':}', ':]'];
    this.nbSmiley = -1;
    this.tabSommeCtrl = this.fb.control(undefined, Validators.required);
    this.challenge1Form = this.fb.group(
      {tabSomme: this.tabSommeCtrl}
    );
    this.couleurAleatoire = '';
  }

  ngOnInit(): void {
    // Challenge 1
    this.tabSommeObs = this.tabSommeCtrl.valueChanges;
  }

  /**
   * Challenge 1
   * Filtre les nombres d'un tableau et en fait la somme
   */
   sum(): void {
    // Code original pour un tableau en dur
    this.nombres = this.arr.filter(Number.isFinite);
    this.somme = this.nombres.reduce((valPrev, valNext) => valPrev + valNext);

    // Pour un tableau entré à la main
    if (this.tabSommeCtrl.value) {
      this.somme = this.tabSommeCtrl.value
        .split(',')
        .filter(Number)
        .map((elt: any) => Number(elt))
        .reduce((valPrev:number, valNext:number) => valPrev + valNext);
    }
  }

  /**
   * Challenge 2
   * Inverse l'orde d'un nombre et met le résultat dans un tableau de nombre
   */
  reverse(): void {
    this.inversion = this.num.toString().split('').reverse().map(Number);
  }

  /**
   * Challenge 3
   * Compte le nombre de smiley valide
   */
  compter(): void {
    let smileyFiltred = [];
    smileyFiltred = this.smileArray.filter(elt => 
      (elt.includes(':') || elt.includes(';')) && (elt.includes(')') || elt.includes('D')));
    this.nbSmiley = smileyFiltred.length;
  }

  /**
   * 
   * Challenge 4
   *  Génère une couleur aléatoire au format RGB
   */
  generer(): void {
    const min = 0;
    const max = 255;
    let rouge = this.getRandomNumber(min, max);
    let vert = this.getRandomNumber(min, max);
    let bleu = this.getRandomNumber(min, max);
    this.couleurAleatoire = 'rgb('+rouge+', '+vert+', '+bleu+')';
  }

  getRandomNumber(min:number, max:number): number {
    return Math.floor(Math.random() * (max - min));
  } 

  /**
   * Efface la réponse du calcul d'un challenge
   */
   reset(event: any): void {
    // Empêche qu'on déclenche la fonction lié au submit d'un formulaire quand on clique sur un bouton dans le formulaire
    event.preventDefault();
    const nameButton = event.currentTarget.name;
    switch(nameButton) {
      case 'reset1':
        this.somme = undefined;
        this.tabSommeCtrl.setValue(undefined);
        break;
      case 'reset2':
        this.inversion = [];
        break;
      case 'reset3':
        this.nbSmiley = -1;
        break;
      case 'reset4':
        this.couleurAleatoire = '';
        break;
      default:
        console.error('Problème dans le switch case Reset!');
    }
  }

  /**
   * Affiche ou cache la solution d'un challenge
   */
  solution(event: any): void {
    const nameButton = event.currentTarget.name;
    switch(nameButton) {
      case 'solution1':
        this.solutionChallenge1 = !this.solutionChallenge1;
        break;
      case 'solution2':
        this.solutionChallenge2 = !this.solutionChallenge2;
        break;
      case 'solution3':
        this.solutionChallenge3 = !this.solutionChallenge3;
        break;
      case 'solution4':
        this.solutionChallenge4 = !this.solutionChallenge4;
        break;
      default:
        console.error('Problème dans le switch case Solution!')
    }
  }
}
