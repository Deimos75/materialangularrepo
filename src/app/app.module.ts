// Angular
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Angular Material
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';

// Composants
import { AppComponent } from './app.component';
import { MatAutocompleteComponent } from './material/mat-autocomplete/mat-autocomplete.component';
import { MergeSwitchComponent } from './merge-switch/merge-switch.component';
import { MapBackComponent } from './map-back/map-back.component';
import { ExpansionPanelComponent } from './material/expansion-panel/expansion-panel.component';
import { MaterialComponent } from './material/material.component';
import { MapComponent } from './map/map.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { ChallengeComponent } from './challenge/challenge.component';
import { MatTableComponent } from './material/mat-table/mat-table.component';
import { MatTableAddRowComponent } from './material/mat-table-add-row/mat-table-add-row.component';
import { FiltresComponent } from './material/mat-select/filtres.component';
import { ConcatPipe } from './concat.pipe';

// Date avec le zéro pour les jours et les mois inférieur à 10
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { MatPaginatorComponent } from './material/mat-paginator/mat-paginator.component';
import { MapTempsComponent } from './map-temps/map-temps.component';
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    MatAutocompleteComponent,
    MergeSwitchComponent,
    MapBackComponent,
    ExpansionPanelComponent,
    MaterialComponent,
    MapComponent,
    HeaderComponent,
    ChallengeComponent,
    MatTableComponent,
    MatTableAddRowComponent,
    FiltresComponent,
    ConcatPipe,
    MatPaginatorComponent,
    MapTempsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatToolbarModule,
    MatTableModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatPaginatorModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'fr' }, ConcatPipe, { provide: MatPaginatorIntl, useClass: MatPaginatorComponent}],
  bootstrap: [AppComponent]
})
export class AppModule { }
