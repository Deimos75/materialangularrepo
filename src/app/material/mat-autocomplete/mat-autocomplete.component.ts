import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-mat-autocomplete',
  templateUrl: './mat-autocomplete.component.html',
  styleUrls: ['./mat-autocomplete.component.css']
})
export class MatAutocompleteComponent implements OnInit {
  options = ['Un', 'Deux', 'Trois'];
  filteredOptions!: Observable<string[]>;
  nombreForm: FormGroup;
  nombreCtrlName: FormControl;
  nombreSelected?: string;

  constructor(private fb: FormBuilder) {
    this.nombreCtrlName = fb.control('');
    this.nombreForm = fb.group({
      nombreCtrlName: this.nombreCtrlName
    });
   }

  ngOnInit(): void {
    this.filteredOptions = this.nombreCtrlName.valueChanges
      .pipe(startWith(''), map(value => value ? this.filtre(value) : this.options.slice()));
  }

  /**
   * Filtre les valeurs saisies dans le input du mat-autocomplete
   * @param value : valeur saisie
   * @returns : option dans le mat-option correspondant à la valeur saisie ou valeur cliquée
   */
  private filtre(value: string): string[] {
    const inputValue = value.toLowerCase();
    const filterValue = this.options.filter(
      option => option.toLowerCase().includes(inputValue)
      );
    return filterValue;
  }

  /**
   * Change la couleur de l'interrupteur et le style du cursor
   * @param event 
   */
  toggleChange(event:MatSlideToggleChange): void {
    this.nombreCtrlName.disabled ? this.nombreCtrlName.enable() : this.nombreCtrlName.disable();
    const elt = document.getElementsByClassName('mat-form-field-flex');
    event.checked ? elt[0].setAttribute('style', 'cursor: not-allowed') : elt[0].removeAttribute('style');
  }

  /**
   * Permet de récuperer le nombre choisi
   */
  valider(): void {
    this.nombreSelected = this.nombreCtrlName.value;
    console.log('===> nombre : ', this.nombreSelected);
  }
  
  /**
   * Permet de vider la liste déroulante
  */
  reset(): void {
    this.nombreCtrlName.reset();
    this.nombreSelected = this.nombreCtrlName.value;
  }
}
