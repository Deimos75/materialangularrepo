import { Component } from '@angular/core';

// Données du tableau (par ligne)
const USER_DATA = [
  {id: 1, name: 'John Smith', occupation: 'Advisor', dateOfBirth: '05/05/1984', age: 36},
  {id: 2, name: 'Muhi Masri', occupation: 'Developer', dateOfBirth: '02/02/1992', age: 28},
  {id: 3, name: 'Peter Adams', occupation: 'HR', dateOfBirth: '01/01/2000', age: 20},
  {id: 4, name: 'Lora Bay', occupation: 'Marketing', dateOfBirth: '03/03/1977', age: 43},
]

// Nom des colonnes (les key correspondent aux clés des ligne USER_DATA)
const COLUMNS_SCHEMA = [
  {
      key: 'name',
      type: 'text',
      label: 'Full Name'
  },
  {
      key: 'occupation',
      type: 'text',
      label: 'Occupation'
  },
  {
    key: 'dateOfBirth',
    type: 'date',
    label: 'Date of Birth'
  },
  {
      key: 'age',
      type: 'number',
      label: 'Age'
  }
]

@Component({
  selector: 'app-mat-table',
  templateUrl: './mat-table.component.html',
  styleUrls: ['./mat-table.component.css']
})
export class MatTableComponent {
  displayedColumns = COLUMNS_SCHEMA.map(elt => elt.key);
  columnsSchema = COLUMNS_SCHEMA;
  dataSource = USER_DATA;
}
