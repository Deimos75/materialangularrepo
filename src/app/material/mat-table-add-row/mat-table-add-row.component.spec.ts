import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatTableAddRowComponent } from './mat-table-add-row.component';

describe('MatTableAddRowComponent', () => {
  let component: MatTableAddRowComponent;
  let fixture: ComponentFixture<MatTableAddRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatTableAddRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatTableAddRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
