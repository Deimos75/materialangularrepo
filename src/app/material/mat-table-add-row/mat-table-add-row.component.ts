import { MatTableComponent } from './../mat-table/mat-table.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mat-table-add-row',
  templateUrl: './mat-table-add-row.component.html',
  styleUrls: ['./mat-table-add-row.component.css']
})
export class MatTableAddRowComponent extends MatTableComponent implements OnInit {

  ngOnInit(): void {
    this.initColonnes();
  }

  addRow(): void {
    const newRow = {id: Date.now(), name: '', occupation: '', dateOfBirth: '', age: 0};
    this.dataSource = [newRow, ...this.dataSource];
  }

  /**
   * Suppression d'une seule ligne
   */
  deleteRow(element: any): void {    
    const indice = this.dataSource.indexOf(element);
    this.dataSource = this.deleteArrElt(this.dataSource, indice);
  }

  /**
   * Active / déactive le mode d'édition
   * Clique "Edit" Au départ element.isEdit n'existe pas => !element.isEdit = true
   * Clique "Done" element.isEdit = true => on le met à false
   * @param element: ligne que l'on modifie
   */
  editSwitch(element: any): void {
    element.dateOfBirth = new Date(element.dateOfBirth);
    // Cas de l'édition
    if (element.isEdit) {
      const diffMs = Date.now() - Date.parse(element.dateOfBirth);
      const annee = this.msToYear(diffMs);
      const indice = this.dataSource.indexOf(element);
      this.dataSource[indice].age = annee;
    }
    element.isEdit = !element.isEdit;
  }

  /**
   * Ajout de la colonne Edit
   */
  initColonnes(): void {
    this.columnsSchema = [...this.columnsSchema, {key: 'edit', type: 'btnEdit', label:''}];
    this.displayedColumns = this.columnsSchema.map(elt => elt.key);
  }

  /**
   * Suppression de l'élément d'un tableau
   * @param tableau   tableau dont on veut supprimer un élément
   * @param indice    indice de l'élément du tableau à supprimer
   * @returns
   */
  deleteArrElt(tableau: any[], indice: number): any[] {
    const tabFin = tableau.slice(indice+1);
    const tabDebut = tableau.slice(0, indice);
    return tabDebut.concat(tabFin);
  }

  /**
   * Converti un nombre de millisecondes en années
   * @param tempsMs Temps en milliseconde à convertir 
   * @returns       Temps en années
   */
  msToYear(tempsMs: number): number {
    return Math.trunc((((tempsMs / 1000) / 3600) / 24) / 365);
  }
}
