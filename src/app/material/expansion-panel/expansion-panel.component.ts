import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.css']
})
export class ExpansionPanelComponent {
  panelOuvert = false;
  @Input() titre?: string;
  @Input() isDescription?: boolean;
  @Input() description?: string;
}
