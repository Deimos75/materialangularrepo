// Type pour tout le multi select
export interface CheckBoxFiltre {
    titreFiltre: string;
    checkBoxCategorie: CheckBoxCategorie[];
    formControlName: string;
  }
  
  // Type pour une catégorie dans le multi select
  export interface CheckBoxCategorie {
    categorieTitre: string;
    categorieValeur: string[];
  }
  
  export interface SelectFiltre {
    titreFiltre: string;
    selectValeur: string[];
    formControlName: string;
  }
  
  export const enum TypeDecisionFiltreEnum {
    TOTAL = 'Totale',
    PARTIEL = 'Partielle',
    REJET = 'Rejet',
    TOUT = 'Tout',
    RESSOURCES = 'Ressources',
    MAINTIEN = 'Maintien',
    ART6 = 'Art 6',
    ART92 = 'Art 9-2',
    PIECES = 'Pièces',
    CADUCITE = 'Caducité',
    INCOMPETENCE = 'Incompétence',
    LIBRE = 'Libre'
  }
  
  export const enum SelectFiltreEnum {
    TOUT = 'Tout',
    OUI = 'Oui',
    NON = 'Non'
  }

export const SELECT_VALEUR_DEFAUT = 'Tout';

export interface CriteriaFiltre {
  selectCriteria: SelectFiltre[];
  checkBoxCriteria: CheckBoxFiltre[];
}

export interface ToutCategorieSelected {
  [key: string]: boolean;
}
