import { AfterViewChecked, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatOptionSelectionChange } from '@angular/material/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { Router } from '@angular/router';
import { ConcatPipe } from 'src/app/concat.pipe';
import { CheckBoxCategorie, CheckBoxFiltre, SelectFiltre, SELECT_VALEUR_DEFAUT, ToutCategorieSelected } from './filtres.model';

@Component({
  selector: 'app-filtres',
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.css']
})
export class FiltresComponent implements OnInit, AfterViewChecked {
  @Input() checkBoxFiltre!: CheckBoxFiltre[];
  @Input() selectFiltre!: SelectFiltre[];
  formFiltre!: FormGroup;
  // Tableau des valeurs sélectionnées (utile pour la sessionStorage)
  checkBoxFiltreSelected!: CheckBoxFiltre[];
  selectFiltreSelected!: SelectFiltre[];
  isToutCategorieSelected: ToutCategorieSelected = {};
  isPanelOpen!: boolean;
  nbSelectionTotal = 0;
  cleSessionStorageFiltreCheckBox!: string;
  cleSessionStorageFiltreSelect!: string;
  nbFiltre!: number;
  largeurFiltre!: number;
  isBlack = false;
  selectTrigger: string[] = [];
  selectDefaultColor: boolean[] = [];
  @ViewChild(MatExpansionPanel) matExpansionPanel!: MatExpansionPanel;

  constructor(private fb: FormBuilder, private concatPipe: ConcatPipe, private route: Router) {}

  ngOnInit() {
    this.cleSessionStorageFiltreCheckBox = 'filtres-checkbox-' + this.route.url;
    this.cleSessionStorageFiltreSelect = 'filtres-select-' + this.route.url;
    this.initForm();
    this.initCheckBoxFiltreSelected();
    if (sessionStorage.getItem(this.cleSessionStorageFiltreCheckBox) || sessionStorage.getItem(this.cleSessionStorageFiltreSelect)) {
      this.getSessionStorage();
    }
    this.nbFiltre = this.checkBoxFiltre.length + this.selectFiltre.length;
    this.largeurFiltre = (1 / (2 + this.nbFiltre)) * 100 - 2;
  }

  ngAfterViewChecked(): void {
    Array.from(this.checkBoxFiltreSelected).forEach((filtre, indexFiltre) => {
      filtre.checkBoxCategorie.forEach((categorie, indexCat) => {
        this.cocheCheckBoxTout(indexFiltre, indexCat, categorie);
      });
    });
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.formFiltre = this.fb.group({});

    // Création des controls
    this.checkBoxFiltre.forEach(filtre => {
      this.formFiltre.addControl(filtre.formControlName, this.fb.control(''));
    });
    this.selectFiltre.forEach(filtre => {
      this.formFiltre.addControl(filtre.formControlName, this.fb.control(''));
    });

    // Sélection par défaut à 'Tout' dans le mat-select
    this.selectFiltre.forEach((filtre: SelectFiltre, indiceFiltre) => {
      this.formFiltre.get(filtre.formControlName)?.setValue(SELECT_VALEUR_DEFAUT);
      this.selectTrigger[indiceFiltre] = SELECT_VALEUR_DEFAUT;
      this.selectDefaultColor[indiceFiltre] = true;
    });
  }

  /**
   * Initialisation de checkBoxFiltreSelected et de selectFiltreSelected
   */
  initCheckBoxFiltreSelected(): void {
    // Permet de copier l'objet this.checkFiltre dans this.checkFiltreSelected sans copier sa référence
    this.checkBoxFiltreSelected = JSON.parse(JSON.stringify(this.checkBoxFiltre));
    this.selectFiltreSelected = JSON.parse(JSON.stringify(this.selectFiltre));

    // Par défaut les valeurs de this.checkFiltreSelected sont mises à vide
    this.clearCheckBoxFiltreSelected();
  }

  /**
   * Vide les valeurs sélectionnées pour les checkboxs et met les valeurs par défaut pour les selects
   */
  clearCheckBoxFiltreSelected(): void {
    this.checkBoxFiltre.forEach((eltFiltre, indexFiltre) =>
      eltFiltre.checkBoxCategorie.forEach((eltCat, indexCat) =>
        eltCat.categorieValeur.forEach(() => {
          this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur = [];
        })
      )
    );

    this.selectFiltre.forEach((filtre: SelectFiltre, indexFiltre) => {
      this.selectFiltreSelected[indexFiltre].selectValeur = [SELECT_VALEUR_DEFAUT];
    });
  }

  /**
   * Quand on clique sur une case à cocher (ou si on fait un setValue)
   * @param event       Permet de savoir si la case est cochée
   * @param titreFiltre Titre du filtre ouvert
   */
  optionChange(event: MatOptionSelectionChange, titreFiltre: string): void {
    const categorieClick = event.source.group.label;
    const textClick = event.source.viewValue;

    // Mise à jour de checkfiltreSelected
    Array.from(this.checkBoxFiltreSelected).forEach((filtre, indexFiltre) => {
      if (filtre.titreFiltre === titreFiltre) {
        filtre.checkBoxCategorie.forEach((categorie, indexCat) => {
          if (categorie.categorieTitre === categorieClick) {
            if (
              event.source.selected &&
              !this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.includes(textClick)
            ) {
              this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.push(textClick);
            } else if (!event.source.selected) {
              const index = this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.indexOf(textClick);
              this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.splice(index, 1);
            }
            this.cocheCheckBoxTout(indexFiltre, indexCat, categorie);
          }
        });
      }
    });
  }

  /**
   * Coche ou décoche "Tout" pour une catégorie
   * @param indexFiltre indice du filtre à vérifier
   * @param indexCat    indice de la catégorie à vérifier
   * @param categorie   categorie à vérifier
   */
  cocheCheckBoxTout(indexFiltre: number, indexCat: number, categorie: CheckBoxCategorie): void {
    const nbSelected = this.checkBoxFiltreSelected[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.length;
    const nbTotalCat = this.checkBoxFiltre[indexFiltre].checkBoxCategorie[indexCat].categorieValeur.length;
    const valueToutCat = 'tout-' + this.concatPipe.transform(categorie.categorieTitre);
    const elt = document.getElementById(valueToutCat);

    if (elt) {
      if (nbSelected === nbTotalCat) {
        elt.firstElementChild?.classList.add('mat-pseudo-checkbox-checked');
      } else {
        elt.firstElementChild?.classList.remove('mat-pseudo-checkbox-checked');
      }
    }
  }

  /**
   * Permet de savoir si "Tout" pour une catégorie est sélectionné
   * Se déclenche en cas de changement sur le mat-option "Tout"
   * @param event
   */
  optionToutChange(event: MatOptionSelectionChange, titreCategorie: string): void {
    this.isToutCategorieSelected[titreCategorie] = event.source.selected;
    console.log('===> this.isToutCategorieSelected[titreCategorie] : ', this.isToutCategorieSelected[titreCategorie]);
  }

  /**
   * Affiche l'état indéterminé dans la checkbox "Tout sélectionner"
   * @param titreFiltre Titre du filtre en cours
   * @returns true si c'est indéterminé
   */
  isIndeterminate(titreFiltre: string): boolean {
    let indiceFiltre = 0;
    let nbCategorieValeurData = 0;
    let nbCategorieValeurSelected = 0;

    // Indice du filtre en cours
    this.checkBoxFiltre.forEach((filtre, indexFiltre) => {
      indiceFiltre = filtre.titreFiltre === titreFiltre ? indexFiltre : 0;
    });

    // On boucle que sur les catégories du filtre en cours (titreFiltre)
    this.checkBoxFiltre[indiceFiltre].checkBoxCategorie.forEach((categorie, indexCategorie) => {
      nbCategorieValeurData += categorie.categorieValeur.length;
      nbCategorieValeurSelected += this.checkBoxFiltreSelected[indiceFiltre].checkBoxCategorie[indexCategorie].categorieValeur.length;
    });

    return nbCategorieValeurData > nbCategorieValeurSelected && nbCategorieValeurSelected > 0;
  }

  /**
   * Permet de cocher / décocher la checkbox "Tout sélectionner" en fonction des mat-option cochés
   * @param titreFiltre Titre du filtre en cours
   * @returns true pour cocher la checkbox
   */
  isChecked(titreFiltre: string): boolean {
    let indiceFiltre = 0;
    let nbCategorieValeurData = 0;
    let nbCategorieValeurSelected = 0;

    // Indice du filtre en cours
    this.checkBoxFiltre.forEach((filtre, indexFiltre) => {
      indiceFiltre = filtre.titreFiltre === titreFiltre ? indexFiltre : 0;
    });

    // On boucle que sur les catégories du filtre en cours (titreFiltre)
    this.checkBoxFiltre[indiceFiltre].checkBoxCategorie.forEach((categorie, indexCategorie) => {
      nbCategorieValeurData += categorie.categorieValeur.length;
      nbCategorieValeurSelected += this.checkBoxFiltreSelected[indiceFiltre].checkBoxCategorie[indexCategorie].categorieValeur.length;
    });
    return nbCategorieValeurData === nbCategorieValeurSelected;
  }

  /**
   * Permet de tout sélectionner / désélectionner quand on clique sur la checkbox "Tout sélectionner"
   * @param change        permet de savoir si la case est cochée
   * @param indiceFiltre  indice du filtre sélectionné
   */
  toutSelectionner(change: MatCheckboxChange, indiceFiltre: number): void {
    if (change.checked) {
      // Récupère toutes les "value" du filtre ouvert
      const toutMap: string[] = [];
      this.checkBoxFiltre[indiceFiltre].checkBoxCategorie.forEach((categorie, indiceCategorie) => {
        toutMap.push('tout-' + this.concatPipe.transform(categorie.categorieTitre));
        categorie.categorieValeur.forEach(categorieValeur => {
          toutMap.push(this.concatPipe.transform(categorieValeur) + '-' + indiceCategorie);
        });
      });

      // Ajout dans le form
      this.formFiltre.get(this.checkBoxFiltre[indiceFiltre].formControlName)?.setValue(toutMap);
    } else {
      // Suppression dans le form
      this.formFiltre.get(this.checkBoxFiltre[indiceFiltre].formControlName)?.setValue([]);
    }
  }

  /**
   * Permet de sélectionner tout une catégorie
   * @param titreFiltre     Titre du filtre ouvert
   * @param titreCategorie  Titre de la catégorie cliquée
   */
  toutSelectionnerCategorie(titreFiltre: string, titreCategorie: string): void {
    let indiceFiltre = -1;
    let indiceCategorie = -1;
    let tout: string[];
    let valeursExistantes: string[];

    // Indice du filtre ouvert
    this.checkBoxFiltre.forEach((filtre: CheckBoxFiltre, indexFiltre) => {
      if (filtre.titreFiltre === titreFiltre) {
        indiceFiltre = indexFiltre;
      }
    });

    // Indide de la catégorie cliquée
    this.checkBoxFiltre[indiceFiltre].checkBoxCategorie.forEach((categorie: CheckBoxCategorie, indexCategorie) => {
      if (categorie.categorieTitre === titreCategorie) {
        indiceCategorie = indexCategorie;
      }
    });

    // Toutes les valeurs de la catégorie sélectionnée
    tout = this.checkBoxFiltre[indiceFiltre].checkBoxCategorie[indiceCategorie].categorieValeur;
    const toutMap = tout.map(elt => this.concatPipe.transform(elt) + '-' + indiceCategorie);

    // Valeurs existantes dans le form
    valeursExistantes = this.formFiltre.get(this.checkBoxFiltre[indiceFiltre].formControlName)?.value;

    // "Tout" est coché
    if (this.isToutCategorieSelected[titreCategorie]) {
      // Ajout dans le form de toutes les valeurs d'une catégorie
      this.formFiltre.get(this.checkBoxFiltre[indiceFiltre].formControlName)?.setValue([...valeursExistantes, ...toutMap]);

    // "Tout" est décoché
    } else {
      // Mise à jour du form
      const valeursRestantes = valeursExistantes.filter(val => !toutMap.includes(val));
      this.formFiltre.get(this.checkBoxFiltre[indiceFiltre].formControlName)?.setValue(valeursRestantes);
    }
  }

  /**
   * Permet d'appliquer les choix des filtres dans le tableau (appel au back) et d'enregistrer les filtres dans la sessionStorage
   */
  valider(): void {
    this.matExpansionPanel.close();
    this.setSessionStorage(true);
  }

  /**
   * Permet de récupérer la sessionStorage pour remplir les filtres du formulaire
   */
  getSessionStorage(): void {
    this.checkBoxFiltreSelected = JSON.parse(sessionStorage.getItem(this.cleSessionStorageFiltreCheckBox) || '{}');
    this.selectFiltreSelected = JSON.parse(sessionStorage.getItem(this.cleSessionStorageFiltreSelect) || '{}');
    let valeursSessionStorageCat: string[] = [];
    const valeursSessionStorage: string[] = [];

    Array.from(this.checkBoxFiltreSelected).forEach((filtre: CheckBoxFiltre) => {
      filtre.checkBoxCategorie.forEach((categorie, indiceCategorie) => {
        valeursSessionStorageCat = categorie.categorieValeur.map(
          categorieValeur => this.concatPipe.transform(categorieValeur) + '-' + indiceCategorie
        );
        valeursSessionStorage.push(...valeursSessionStorageCat);
        this.formFiltre.get(filtre.formControlName)?.setValue(valeursSessionStorage);
      });
    });

    Array.from(this.selectFiltreSelected).forEach((filtre: SelectFiltre, indiceFiltre) => {
      this.formFiltre.get(filtre.formControlName)?.setValue(filtre.selectValeur.toString());
      this.selectTrigger[indiceFiltre] = filtre.selectValeur.toString();
      if (this.selectTrigger[indiceFiltre] === SELECT_VALEUR_DEFAUT) {
        this.selectDefaultColor[indiceFiltre] = true;
      } else {
        this.selectDefaultColor[indiceFiltre] = false;
      }
    });
  }

  /**
   * Permet d'écrire dans la sessionStorage (enregistre les filtres avec une clé qui dépend de l'url de la page courante)
   * @param save si true alors on enregistre this.checkBoxFiltreSelected dans la sessionStorage, si false alors on vide la sessionStorage
   */
  setSessionStorage(save: boolean): void {
    if (save) {
      sessionStorage.setItem(this.cleSessionStorageFiltreCheckBox, JSON.stringify(this.checkBoxFiltreSelected));
      sessionStorage.setItem(this.cleSessionStorageFiltreSelect, JSON.stringify(this.selectFiltreSelected));
    } else {
      sessionStorage.removeItem(this.cleSessionStorageFiltreCheckBox);
      sessionStorage.removeItem(this.cleSessionStorageFiltreSelect);
    }
  }

  /**
   * Permet de vider les filtres et la sessionStorage et this.checkBoxFiltreSelected / this.selectFiltreSelected
   */
  reset(): void {
    this.formFiltre.reset();
    this.setSessionStorage(false);
    this.checkBoxFiltreSelected.forEach((filtre: CheckBoxFiltre) => {
      this.formFiltre.get(filtre.formControlName)?.reset();
      filtre.checkBoxCategorie.forEach(categorie => {
        categorie.categorieValeur = [];
      });
    });

    this.selectFiltreSelected.forEach((filtre: SelectFiltre, indiceFiltre) => {
      this.formFiltre.get(filtre.formControlName)?.setValue(SELECT_VALEUR_DEFAUT);
      filtre.selectValeur = [SELECT_VALEUR_DEFAUT];
      this.selectDefaultColor[indiceFiltre] = true;
      this.selectTrigger[indiceFiltre] = SELECT_VALEUR_DEFAUT;
    });
  }

  /**
   * Permet de griser les lignes du tableau quand le panel des filtres est ouvert
   */
  openPanel(): void {
    const eltTab = document.getElementsByClassName('mat-table');
    const eltHeader = document.getElementsByClassName('mat-header-row');
    eltTab[0].classList.add('grise-tableau');
    eltHeader[0].classList.add('grise-header');
    this.isPanelOpen = true;
  }

  /**
   * Permet d'enlever le gris sur les lignes du tableau quand le panel des filtres est fermé
   */
  closePanel(): void {
    const eltTab = document.getElementsByClassName('mat-table');
    const eltHeader = document.getElementsByClassName('mat-header-row');
    eltTab[0].classList.remove('grise-tableau');
    eltHeader[0].classList.remove('grise-header');
    this.isPanelOpen = false;
  }

  /**
   * Calcul du nombre de cases cochés pour un filtre
   * @param indiceFiltre
   * @returns
   */
  getTotalByFiltre(indiceFiltre: number): number {
    let totalFiltre = 0;
    this.checkBoxFiltreSelected[indiceFiltre].checkBoxCategorie.forEach((categorie: CheckBoxCategorie) => {
      totalFiltre += categorie.categorieValeur.length;
    });
    return totalFiltre;
  }

  /**
   * Calcul du nombre de filtre (liste déroulante) qui ont des cases cochées
   * ou des mat-select qui ont une valeur différente de la valeur par défaut
   * @returns
   */
  getTotalNbFiltre(): number {
    const totalFiltreCheck: number[] = [];
    let totalFiltreSelect = 0;
    let nbFiltre = 0;
    this.checkBoxFiltreSelected.forEach((filtre: CheckBoxFiltre, indiceFiltre) => {
      totalFiltreCheck[indiceFiltre] = 0;
      filtre.checkBoxCategorie.forEach((categorie: CheckBoxCategorie) => {
        totalFiltreCheck[indiceFiltre] += categorie.categorieValeur.length;
      });
    });

    this.selectFiltreSelected.forEach((filtre: SelectFiltre) => {
      if (filtre.selectValeur[0] !== SELECT_VALEUR_DEFAUT) {
        totalFiltreSelect++;
      }
    });

    nbFiltre = totalFiltreCheck.filter(filtre => filtre > 0).length + totalFiltreSelect;
    return nbFiltre;
  }

  /**
   * Mise à jour de this.selectFiltreSelected
   * @param filtreClick
   * @param valeurClick
   * @param indiceFiltre
   */
  selectOptionClick(filtreClick: SelectFiltre, valeurClick: string, indiceFiltre: number): void {
    // Mise à jour de this.selectFiltreSelected
    this.selectFiltreSelected.forEach((filtre: SelectFiltre) => {
      if (filtreClick.titreFiltre === filtre.titreFiltre) {
        filtre.selectValeur = [valeurClick];
      }
    });

    // Valeur sélectionnnée (affichage + style)
    this.selectTrigger[indiceFiltre] = valeurClick;
    this.selectDefaultColor[indiceFiltre] = false;
  }
}
