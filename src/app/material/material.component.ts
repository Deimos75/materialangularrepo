import { CheckBoxCategorie, CheckBoxFiltre, SelectFiltre, SelectFiltreEnum, TypeDecisionFiltreEnum } from './mat-select/filtres.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  // Tous les filtres de type checkbox (multi select)
  checkBoxFiltre!: CheckBoxFiltre[];
  
  // Valeurs des différentes catégories de type checkbox
  checkBoxCategorieFiltre1!: CheckBoxCategorie[];
  checkBoxCategorieFiltre2!: CheckBoxCategorie[];

  // Filtres de type Select 
  selectFiltre!: SelectFiltre[];
  

  ngOnInit(): void {
    this.initData();
  }

  /**
   * Initialise les listes déroulantes des filtres
   */
  initData(): void {
    // Valeurs des catégories par filtre
    this.checkBoxCategorieFiltre1 = [
      {
        categorieTitre: 'Totale',
        categorieValeur: [TypeDecisionFiltreEnum.RESSOURCES, TypeDecisionFiltreEnum.MAINTIEN, TypeDecisionFiltreEnum.ART6, TypeDecisionFiltreEnum.ART92]
      },
      {
        categorieTitre: 'Partielle',
        categorieValeur: [TypeDecisionFiltreEnum.RESSOURCES, TypeDecisionFiltreEnum.MAINTIEN]
      },
      {
        categorieTitre: 'Rejet',
        categorieValeur: [TypeDecisionFiltreEnum.RESSOURCES, TypeDecisionFiltreEnum.PIECES, TypeDecisionFiltreEnum.CADUCITE, TypeDecisionFiltreEnum.INCOMPETENCE, TypeDecisionFiltreEnum.LIBRE]
      }
    ];

    // Valeurs de chaque filtre du type checkbox
    this.checkBoxFiltre = [
      {
        titreFiltre: 'Types de décisions',
        checkBoxCategorie: this.checkBoxCategorieFiltre1,
        formControlName: 'checkBoxF1'
      }
    ];

    // Valeurs de chaque filtre du type select
    this.selectFiltre = [
      {
        titreFiltre: 'Commission d\'office',
        selectValeur: [SelectFiltreEnum.TOUT, SelectFiltreEnum.OUI, SelectFiltreEnum.NON],
        formControlName: 'selectF1'
      },
      {
        titreFiltre: 'Urgence',
        selectValeur: [SelectFiltreEnum.TOUT, SelectFiltreEnum.OUI, SelectFiltreEnum.NON],
        formControlName: 'selectF2'
      }
    ];
  }
}
