import { TestBed } from '@angular/core/testing';
import { AfterViewInit, Component, OnInit, AfterViewChecked, AfterContentInit, AfterContentChecked } from '@angular/core';
import { Subject, of, Observable, BehaviorSubject } from 'rxjs';
import { concatMap, mergeMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-map-temps',
  templateUrl: './map-temps.component.html',
  styleUrls: ['./map-temps.component.css']
})
export class MapTempsComponent implements OnInit {

  subjectNombre$ = new BehaviorSubject<number>(-1);
  subjectTexte$ = new BehaviorSubject<string>('');
  compteurNombre: number;
  intervalIdNombre: any;
  intervalIdTexte: any;
  lettres = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' , 'j', 'k' , 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  compteurTexte: number;
  resultatSwitch: any[] = [];
  resultatConcat: any[] = [];
  resultatMerge: any[] = [];

  constructor() { }

  ngOnInit(): void {
    //setTimeout(() => this.reset(), 10);
    this.compteurNombre = this.compteurTexte = 0;
    //this.switch();
    //this.concat();
    this.merge();
  }

  start(): void {
    this.intervalIdNombre = setInterval(() => this.emission('nombre'), 1000);
    this.intervalIdTexte = setInterval(() => this.emission('texte'), 500);
  }

  stop(): void {
    clearInterval(this.intervalIdNombre);
    clearInterval(this.intervalIdTexte);
  } 

  reset(): void {
    console.log('===> RESET');
    this.compteurNombre = this.compteurTexte = 0;
    this.subjectNombre$.next(0);
    this.subjectTexte$.next('a');
  }

  emission(param: string): void {
    switch (param) {
      case 'nombre':
        this.subjectNombre$.next(this.compteurNombre);
        this.compteurNombre++;
        break;
      case 'texte':
        this.subjectTexte$.next(this.lettres[this.compteurTexte]);
        this.compteurTexte < 25 ? this.compteurTexte++ : this.compteurTexte = 0;
        break;
      default:
        console.error('===> Problème avec le paramètre: ', param);
    }
  }

  switch(): void {
    this.subjectNombre$.pipe(switchMap((nombre: number) => {
      console.log('\n===> SWITCH');
      console.log('===> nombre : ', nombre);
      this.resultatSwitch.push(nombre);
      console.log('===> resultatSwitch : ', this.resultatSwitch);
      return this.subjectTexte$
    }))
    .subscribe((lettre: string) => {
      console.log('======> lettre : ', lettre);
      this.resultatSwitch.push(lettre);
      console.log('======> resultatSwitch : ', this.resultatSwitch);
      });
  }

  concat(): void {
    this.subjectNombre$.pipe(concatMap((nombre: number) => {
      console.log('\n===> CONCAT');
      console.log('===> nombre : ', nombre);
      this.resultatConcat.push(nombre);
      console.log('===> resultatConcat : ', this.resultatConcat);
      return this.subjectTexte$
    }))
    .subscribe((lettre: string) => {
      console.log('======> lettre : ', lettre);
      this.resultatConcat.push(lettre);
      console.log('======> resultatConcat : ', this.resultatConcat);
      });
  }

  merge(): void {
    this.subjectNombre$.pipe(mergeMap((nombre: number) => {
      console.log('\n===> MERGE');
      console.log('===> nombre : ', nombre);
      this.resultatMerge.push(nombre);
      console.log('===> resultatMerge : ', this.resultatMerge);
      return this.subjectTexte$
    }))
    .subscribe((lettre: string) => {
      console.log('======> lettre : ', lettre);
      this.resultatMerge.push(lettre);
      console.log('======> resultatMerge : ', this.resultatMerge);
      });
  }




}
