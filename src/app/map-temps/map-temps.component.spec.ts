import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapTempsComponent } from './map-temps.component';

describe('MapTempsComponent', () => {
  let component: MapTempsComponent;
  let fixture: ComponentFixture<MapTempsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapTempsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapTempsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
